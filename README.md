# Roberto Api

> Generic ZPL print API.

## Requirements

```sh
Java 11
Plugin Lombok
```

## OS X & Linux Installation:

**Java 11 - SDKMAN:**

```sh
https://sdkman.io/install
sdk i java 11.0.5-open
```

**Lombok plugin:**

```sh
Intellij: https://projectlombok.org/setup/intellij
Eclipse : https://projectlombok.org/setup/eclipse
VSCode : https://projectlombok.org/setup/vscode
```

## Development Configuration:

Access the project root folder:

**Compile the project:**

```sh
sdk use java 11.0.2-open
./mvnw clean package
```

**Run coverage:**

```sh
sdk use java 11.0.2-open
./mvnw clean install jacoco:report
```

**Run the project with local configuration:**

```sh
sdk use java 11.0.2-open
./mvnw clean spring-boot:run -Dspring-boot.run.profiles=local
```

**Execute the project**

```
mvn spring-boot:run
```

**Swagger**

```
....:8080/roberto/swagger-ui.html
```





